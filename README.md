# Why Emacs ?
Flowchart to help you decide if you really need Emacs or not.

# License
[<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />](http://creativecommons.org/licenses/by-sa/4.0/)

"Why Emacs" work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


You are free to:

Share — copy and redistribute the material in any medium or format  
Adapt — remix, transform, and build upon the material  
for any purpose, even commercially.  

**Under the following terms:**

**Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made.  
You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.  
**ShareAlike** — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

This license is acceptable for Free Cultural Works.

The licensor cannot revoke these freedoms as long as you follow the license terms.